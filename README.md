# Wine-WeChat
微信 WINE 安装

## 安装
```bash
# 方法一（git 远程安装）
wineapp -i https://jihulab.com/wineapp/wine-wechat.git

# 方法二 (中心库 远程安装)
wineapp -i com.qq.weixin.wine

# 方法三 本地安装
git clone https://jihulab.com/wineapp/wine-wechat.git
cd wine-wechat
wineapp -i
```

## 中心库
- https://jihulab.com/wineapp/winecenter.git
```bash
# 搜索
wineapp search com.qq.weixin.wine
```
