#!/usr/bin/env bash

#   Copyright (C) Shantien Wine Team
#
#   Author:     Jetsung Chan <jetsungchan@gmail.com>

init() {
    APP_NAME="Wine-WeChat"
    APP_VER="3.6.0"
    START_SHELL_PATH="${HOME}/.wineapp/tools/run.sh"

    EXEC_PATH="c:/Program Files (x86)/Tencent/WeChat/WeChat.exe"
    UNINSTALL_PATH="c:/Program Files (x86)/Tencent/WeChat/Uninstall.exe"

    INSTALL_QUIET="/S"

    export MIME_TYPE=""

    EXPORT_ENVS=""
    if [ -n "${EXPORT_ENVS}" ];then
        export ${EXPORT_ENVS}
    fi
}

exec() {
    RUN_PATH="${EXEC_PATH}"

    REMOVE=$(echo "$@" | grep "\-\-remove")
    [[ -z "${REMOVE}" ]] || RUN_PATH="${UNINSTALL_PATH}"

    if [ -n "${RUN_PATH}" ];then
        ${START_SHELL_PATH} ${APP_NAME} ${APP_VER} "${RUN_PATH}" "$@"
    else
        ${START_SHELL_PATH} ${APP_NAME} ${APP_VER} "uninstaller.exe" "$@"
    fi
}

main() {
    init

    [[ -z "${1}" ]] || exec "$@" #2>&1 | tee -a ${HOME}/install.log
}

main "$@" || exit 1
